Pod::Spec.new do |spec|

  spec.name         = "AShieldAuthKit"
  spec.version      = "0.1.0"
  spec.summary      = "User Authentication through Cellular and WIFI data"
  spec.description  = "User Authentication through Cellular and WIFI data"
  spec.homepage     = "https://bitbucket.org/_jubinjacob/ashieldauthkitsdk/src/master/"
  spec.license      = { :type => "MIT", :file => "LICENSE" }
  spec.author             = { "Jubin Jacob" => "mailtojubinjacob@gmail.com" }
  spec.platform     = :ios
  spec.platform     = :ios, "13.0"
  spec.vendored_frameworks = 'AShieldAuthKit.xcframework'
  spec.source       = { :git => "https://bitbucket.org/_jubinjacob/ashieldauthkitsdk/src/master/AShieldAuthKit.zip", :branch => "master" }
  spec.public_header_files = "AShieldAuthKit.xcframework/*/AShieldAuthKit.framework/Headers/*.h"
  spec.source_files = "AShieldAuthKit.xcframework/*/AShieldAuthKit.framework/Headers/*.h"

end
